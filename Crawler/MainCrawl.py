import os
from cv2 import cv2

from Crawler.Module.Crawltor import Crawltor

crawler = Crawltor()

images_source = 'F:\TrafficSign\Crawler\Input\Image'


def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))
        if img is not None:
            images.append(img)
    return images


def load_videos_from_forlder(folder):
    caps = []
    for filename in os.listdir(folder):
        path = os.path.join(folder, filename)
        cap = cv2.VideoCapture(path)
        if cap is not None:
            caps.append(cap)
    return caps


def load_frames_from_videos(cap, minFrameDistance=5):
    frameIndex = 0
    lastSave = -99
    success, image = cap.read()
    frames = []
    success = True
    while success:

        frameIndex += 1
        if frameIndex - lastSave < minFrameDistance:
            continue
        success, image = cap.read()
        if image is None:
            break
        frames.append(image)
        lastSave = frameIndex

    return frames


# for image in load_images_from_folder(images_source):
#     crawler.crawl_image(image)

caps = load_videos_from_forlder('Input/Video')
index_video = 1
for cap in caps:
    crawler.crawl_video(cap,minFrameDistance=0)
    print('Finish : '+str(index_video)+'/'+str(len(caps)))
    index_video+=1
