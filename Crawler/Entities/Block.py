import json
import xml.etree.cElementTree as ET
from cv2 import cv2


class Block:
    def __init__(self, file_name, image_detect):
        self.filename = file_name
        self.image_detected = image_detect
        self.detectedobjects = self.image_detected.detected_objects

    def toJSON(self):
        return json.dumps(self.detectedobjects, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def saveXml(self, _folder, _path,minScore = .5, outForder = 'Xml', _database='Unknown', _segmented=0):
        _filename = self.filename+'.jpg'
        _image = self.image_detected.image
        _width, _height = _image.shape[0], _image.shape[1]

        annotation = ET.Element("annotation")
        folder = ET.SubElement(annotation, "folder")
        folder.text = _folder
        filename = ET.SubElement(annotation, "filename")
        filename.text = _filename
        path = ET.SubElement(annotation, "path")
        path.text = _path+'/'+_filename
        source = ET.SubElement(annotation, "source")
        ET.SubElement(source, "database").text = _database
        size = ET.SubElement(annotation, "size")
        ET.SubElement(size, "width").text = str(_width)
        ET.SubElement(size, "height").text = str(_height)
        ET.SubElement(size, "depth").text = str(3)
        segmented = ET.SubElement(annotation, "segmented")
        segmented.text = str(_segmented)

        for _object in self.image_detected.detected_objects:
            if _object.score > minScore:
                _box = _object.box
                ymin, xmin, ymax, xmax = _box
                object = ET.SubElement(annotation, "object")
                ET.SubElement(object, "name").text = str(int(_object.label['id']) -1) #phải - 1 để phù hợp với file Xml_to_csv cũ đã sửa
                ET.SubElement(object, "pose").text = 'Unspecified'
                ET.SubElement(object, "truncated").text = str(1)
                ET.SubElement(object, "difficult").text = str(0)
                bndbox = ET.SubElement(object, "bndbox")
                ET.SubElement(bndbox, "xmin").text = str(int(xmin))
                ET.SubElement(bndbox, "ymin").text = str(int(ymin))
                ET.SubElement(bndbox, "xmax").text = str(int(xmax))
                ET.SubElement(bndbox, "ymax").text = str(int(ymax))
        tree = ET.ElementTree(annotation)
        tree.write(outForder+'/'+self.filename+".xml")
        cv2.imwrite('Images/' + self.filename + '.jpg', self.image_detected.image)

    def save(self):
        with open('Json/' + self.filename + '.json', 'w') as outfile:
            outfile.write(self.toJSON())
            outfile.close()
        cv2.imwrite('Images/' + self.filename + '.jpg', self.image_detected.image)