import os
import glob
import os
import xml.etree.ElementTree as ET


import random


import cv2

train_ratio = 0.7
signs = {}
added_train_signs = {}


def load_xmls(xml_path):
    tree = ET.parse(xml_path)
    root = tree.getroot()
    object_type = None
    for member in root.findall('object'):
        object_type = int(member[0].text)
        if object_type not in signs:
            signs[object_type] = 0

        signs[object_type] += 1


def save_xmls(xml_path):
    tree = ET.parse(xml_path)
    root = tree.getroot()
    object_type = None
    for member in root.findall('object'):
        object_type = int(member[0].text)
    if object_type not in added_train_signs:
        added_train_signs[object_type] = 0


    if object_type is None:
        print('None error : {}'.format(xml_path.replace('Xml/', '')))
        return
    if added_train_signs[object_type] > signs[object_type] * train_ratio:
        save_path = 'F:\TrafficSign\Crawler\Xml\Test/'
    else:
        save_path = 'F:\TrafficSign\Crawler\Xml\Train/'
        added_train_signs[object_type] += 1
    save_path += xml_path.replace('Xml/', '')
    tree.write(save_path)


def randomsave_xml(paths):
    random.shuffle(paths)
    train = 0

    for path in paths:
        save_path = 'F:\TrafficSign\Crawler\Xml\Train/'
        if train > len(paths)*train_ratio:
            save_path = 'F:\TrafficSign\Crawler\Xml\Test/'
        else:
            train += 1
        tree = ET.parse(path)
        save_path += path.replace('Xml/', '')
        tree.write(save_path)


def load_xmls_from_folder(folder):
    xmls = []
    for filename in os.listdir(folder):
        if '.xml' in filename:
            xmls.append('Xml/' + filename)
    return xmls


paths = load_xmls_from_folder('Xml')
randomsave_xml(paths)
# sum = 0
# for sign in signs:
#     sum += signs[sign]
#     print(' bien {} : {} + {} = {}'.format(sign,added_train_signs[sign],signs[sign]-added_train_signs[sign] ,signs[sign]))

# print('sum = {}'.format(sum))
