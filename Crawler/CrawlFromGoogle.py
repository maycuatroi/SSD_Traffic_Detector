import os
from cv2 import cv2

from Crawler.Module.Crawltor import Crawltor
from Crawler.Module.MyBrower import MyBrower

crawler = Crawltor()
url = 'https://www.google.com/search?q=stop+sign&client=firefox-b-ab&tbm=isch&tbs=rimg:CQX67yzScK6TIjh3rXq96dydMgNH0vHVKM83ZC4vNqODFEJblZ_12Nv1Sf4Zim8Sypu_1_1o9XZctiGO37Dv3z0xItbyioSCXeter3p3J0yESo_1SZMVoYpRKhIJA0fS8dUozzcRw9gb3w4J-coqEglkLi82o4MUQhH0fRv9dm5wcyoSCVuVn_1Y2_1VJ_1ETSSFKRnZ8AuKhIJhmKbxLKm7_18RYhdDrV1X8kkqEgmj1dly2IY7fhHOGfINp_1oGZSoSCcO_1fPTEi1vKEUNLC3rA2L2p&tbo=u&sa=X&ved=0ahUKEwiAt7Djpc7YAhUHVZQKHVR6BEQQ9C8IHw&biw=1454&bih=891&dpr=1#imgrc=_'
brower = MyBrower()
urls = MyBrower.brower.get_all_image_url(url)
url_len = len(urls)
indext = 0

for url in urls:
    indext += 1
    image = MyBrower.brower.crawl_image(url)
    if image is not None:
        crawler.crawl_image(image)

MyBrower.brower.driver.close()
