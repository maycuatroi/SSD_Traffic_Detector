import time

from Crawler.Entities.Block import Block
from Modules.Module.SSD_Detector import SSD_Detector


class Crawltor:
    def __init__(self):
        self.detector = SSD_Detector('traffic_sign_graph')


    def crawl_video(self,cap,minFrameDistance = 5):
        frameIndex = 0
        lastSave = -99

        while True:
            ret, image_np = cap.read()
            if image_np is None:
                break
            frameIndex += 1
            if frameIndex - lastSave < minFrameDistance:
                continue
            imagedetected = self.detector.detect_object(image_np)
            TrueRatio = False

            for _object in imagedetected.detected_objects:
                if _object.BoxRatio() < 2 and _object.score > .5:
                    TrueRatio = True

            if TrueRatio:
                lastSave = frameIndex
                fillename = str(frameIndex)+'_'+ str(time.time()).replace('.', ' ')
                block = Block(fillename, imagedetected)
                block.saveXml('Images', 'Crawler/Images')



    def crawl_image(self,image_np):
        imagedetected = self.detector.detect_object(image_np)
        TrueRatio = False

        for _object in imagedetected.detected_objects:
            if _object.BoxRatio() < 1.5 and _object.score > .5:
                TrueRatio = True

        if TrueRatio:
            fillename = str(time.time()).replace('.', ' ')
            block = Block(fillename, imagedetected)
            block.saveXml('Images', 'Crawler/Images')
