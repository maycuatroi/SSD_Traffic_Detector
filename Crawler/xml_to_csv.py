import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET


def xml_to_csv(path):
    xml_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        tree = ET.parse(xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            value = (root.find('filename').text,
                     int(root.find('size')[0].text),
                     int(root.find('size')[1].text),
                      'bienbao'+member[0].text,
                     #bienbao,
                     int(member[4][0].text),
                     int(member[4][1].text),
                     int(member[4][2].text),
                     int(member[4][3].text)
                     )
            xml_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']

    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df,xml_list

def load_images_from_folder(folder):
    files_name = []
    file_type='.jpg'
    for filename in os.listdir(folder):
        if file_type in filename:
            file_name = filename.replace('.jpg', '').replace('.JPG','')
            #file_location = folder+'/'+file_name+''+file_type
            files_name.append(file_name+file_type)
    return files_name

def main():
    existed_files = []
    for directory in ['Xml']:
        xml_path = os.path.join(os.getcwd(), '{}'.format(directory))
        image_path = os.path.join(os.getcwd(), 'images')
        xml_df,xml_list = xml_to_csv(xml_path)
        xml_df.to_csv('data/{}_labels.csv'.format(directory), index=None)
        files = load_images_from_folder(image_path)
        for xml in xml_list:
            existed_files.append(xml[0])
    del_list=[]
    for file in files:
        if file not in existed_files:
            del_list.append(file.replace('.jpg',''))
    for file in del_list:
            os.remove('Images/'+file+'.jpg')
            os.remove('Xml/'+file+'.xml')






main()
