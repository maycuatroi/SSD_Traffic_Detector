import os

import cv2
import numpy as np
import tensorflow as tf
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

MODEL_NAME = 'traffic_sign_graph'
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

PATH_TO_LABELS = 'data/traffic_sign_label.pbtxt'

NUM_CLASSES = 545

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename), cv2.cv2.IMREAD_ANYCOLOR)
        if img is not None:
            images.append(img)
    return images


def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)


IMAGE_SIZE = (12, 8)

i = 0
with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
        images = load_images_from_folder('G:\Python\Google_Image_DataSet\Images')
        for image_np in images:
            image_np_expanded = np.expand_dims(image_np, axis=0)

            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')
            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            vis_util.visualize_boxes_and_labels_on_image_array(
                image_np,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=5, min_score_thresh=.5)

            # print(classes[0][0])
            # print(scores[0][0])

            im_width, im_height = image_np.shape[0], image_np.shape[1]
            for i in range(0, np.squeeze(boxes).shape[0]):
                print(boxes.shape[0])
                if scores is None or scores[0][i] > .5:
                    box = tuple(boxes[0][i].tolist())
                    ymin, xmin, ymax, xmax = box
                    xmin *= im_height
                    xmax *= im_height
                    ymin *= im_width
                    ymax *= im_width
                    # cv2.rectangle(image_np, (int(xmin), int(ymin)), (int(xmax), int(ymax)), (255, 255, 255))
                    croped_image = image_np[int(ymin):int(ymax), int(xmin):int(xmax)]
                    # print(str(int(xmin))+"\t"+str(int(ymin))+"\t"+str(int(ymax-ymin))+"\t"+str(int(xmax-xmin)))
                    cv2.imshow('Croped', croped_image)

            img = cv2.resize(image_np, (800, 600))

            cv2.imshow('', img)
            cv2.waitKey(0)

cv2.destroyAllWindows()
