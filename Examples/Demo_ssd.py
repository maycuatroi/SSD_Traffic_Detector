import json
import os
import sys
import time

import cv2
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Modules.Module.Detector import Detector
from argparse import ArgumentParser


def build_args_parser():
    parser = ArgumentParser()
    parser.add_argument("-v", "--video", type=str, help="Path to video", required=False)
    parser.add_argument("-vw", "--videowrite", type=str, help="Path to video writeout", required=False)

    parser.add_argument("-m", "--model_path", type=str, help="Path to model folder", required=True)

    return parser


args = build_args_parser().parse_args()
video_path = args.video
video_out_patch = args.videowrite
if video_path is not None:
    cap = cv2.VideoCapture(video_path)
else:
    cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
resize_ratio = 4 / 3
out = None
# cv2.VideoWriter('output.avi', fourcc, 40.0, (size[0], size[1]))
detector = Detector(args.model_path)
frameindex = -1
detectAbleFrame = 0
points = []
lines = []
frameBuffered = []
framebufferSize = 15


def process_predict_object(object):
    print(str(object.label['name']) + ' : ' + str(object.score))


def rotateImage(image, angle):
    """
    Rotate image
    :param image: image mat
    :param angle:  rotate value
    :return: image rotated mat
    """
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result


out_data = {}


def smooth_frames(frames, selected_traffic=None):
    """
    detect image in multi continuous frames
    :param frames: continuous frames
    :param selected_traffic: id of selected traffic sign
    :return:
    """
    points = []
    trurth_objects = []
    predict_objects = []
    for frame in frames:
        frame_trurth_objects = []
        for object in frame.detected_objects:
            point = object.GetCentroid()
            if object.score >= .5 and (object.label['id'] == selected_traffic or selected_traffic is None):
                frame_trurth_objects.append(object)
                points.append([object.label['name'], point])
                trurth_objects.append(object)

        if len(frame_trurth_objects) == 0:
            predict_object = frame.detected_objects[0]
            for point in points:
                if predict_object.isInside(point[1]) and predict_object.BoxRatio() < 2:
                    # object dự đoán được đặt ở đây
                    predict_objects.append(predict_object)
                    mat = predict_object.visualize(frame.image)
                    # cv2.imshow('predict', mat)
                    # cv2.waitKey(1)
    for object in predict_objects:
        points.append([object.label['name'], object.GetCentroid()])

    all_objects = predict_objects + trurth_objects

    for object in all_objects:
        out_data[object.frame]['objects'] = [object.to_dict()]


i = 0
length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
for j in range(length):
    out_data[j] = {'fps': 0, 'objects': []}
    out_data[j]
while True:
    start_time = time.time()
    ret, image_np = cap.read()
    frameindex += 1
    if image_np is None:
        break
    i += 1
    imagedetected = detector.detect_object(image_np, frameindex)

    detected_objects = imagedetected.detected_objects
    # out_data[frameindex] = [detected_object.to_dict() for detected_object in detected_objects]
    # image_np = imagedetected.visualize()
    # cv2.imshow('frame', image_np)
    print(str(frameindex * 100 / length) + " %")
    if len(frameBuffered) > framebufferSize:
        frameBuffered.pop(0)
        # cv2.imshow('', frameBuffered[framebufferSize - 1].image)

    frameBuffered.append(imagedetected)

    smooth_frames(frameBuffered)  # sửa dòng này chọn loại biển
    fps = round(1 / (time.time() - start_time))

    out_data[frameindex]['fps'] = fps


cap.release()
if out is not None:
    out.release()
cv2.destroyAllWindows()
open('D:\Github\SSD_Traffic_Detector\Files\{}_frame.json'.format(framebufferSize), 'w').write(
    json.dumps(out_data, indent=4))
print("EXIT !")
