import json
from collections import namedtuple

import cv2
import matplotlib.pyplot as plt

from Modules.Entities.DetectedObject import DetectedObject

traffic_dict = {1: 'bien bao khac', 2: 'stop', 3: 're trai', 4: 're phai', 5: 'cam re trai', 6: 'cam re phai',
                7: 'duong 1 chieu', 8: 'speed limit', 9: 'minimum 30', 10: 'end minimum 30', 11: 'bien soc trang'}
lines = open(r'D:\Github\SSD_Traffic_Detector\Files\new_answer.txt', 'r').read().split('\n')

# print(lines)
true_objects = {}
for line in lines:
    vars = line.split(' ')
    if len(vars) != 6:
        print('ERROR : {}'.format(vars))
        continue
    vars = [int(var) for var in vars]
    label = {'id': int(vars[1]) + 1, 'name': traffic_dict[int(vars[1]) + 1]}

    bbox = [vars[3], vars[2], vars[5], vars[4]]
    if vars[0] not in true_objects:
        true_objects[vars[0]] = [DetectedObject(label, 1.0, bbox)]
    else:
        true_objects[vars[0]].append(DetectedObject(label, 1.0, bbox))
predicted_frames = json.loads(open(r'D:\Github\SSD_Traffic_Detector\Files\15_frame.json').read())
predicted_objects = {}
for frame in predicted_frames:
    # for obj in predicted_frames[frame]:
    objs = predicted_frames[frame]['objects']
    frame = int(frame)
    for obj in objs:
        id = obj['label']
        score = obj['score']
        bbox = obj['bbox']
        obj = DetectedObject(id, score, bbox)
        if frame not in predicted_objects:
            predicted_objects[frame] = [obj]
        else:
            predicted_objects[frame].append(obj)
union = 0
overlap = 0


def compute_area(box):
    dx = box.xmax - box.xmin
    dy = box.ymax - box.ymin
    return dx * dy


Rectangle = namedtuple('Rectangle', 'xmin ymin xmax ymax')


def area(a, b):  # returns None if rectangles don't intersect
    dx = min(a.xmax, b.xmax) - max(a.xmin, b.xmin)
    dy = min(a.ymax, b.ymax) - max(a.ymin, b.ymin)
    if (dx >= 0) and (dy >= 0):
        return dx * dy
    else:
        return 0


cap = cv2.VideoCapture(r"D:\Github\SSD_Traffic_Detector\QuestionVideo.avi")

ious = {}
_overlap = {}
_union = {}
true_positives = {}
false_positives = {}
predict_times = {}
annotation_true_positive = {}
for id in traffic_dict:
    ious[id] = []
    _union[id] = []
    _overlap[id] = []
    true_positives[id] = 0
    false_positives[id] = 0
    predict_times[id] = 0
    annotation_true_positive[id] = 0
index = 0
presion = []
thresh_hold = 0.5
recall = []
while True:
    ret, mat = cap.read()
    if not ret:
        break
    index += 1
    true_object = None

    true_rect_dict = {}
    predict_rect_dict = {}
    frame_ious = {}
    for id in traffic_dict:
        frame_ious[id] = []
        true_rect_dict[id] = []
        predict_rect_dict[id] = []
    if index in true_objects:
        for true_object in true_objects[index]:
            annotation_true_positive[true_object.label['id']] += 1
            true_rect = Rectangle(true_object.box[0], true_object.box[1], true_object.box[2], true_object.box[3])
            true_rect_dict[true_object.label['id']].append(true_rect)
            # true_object.visualize(mat)
    if index in predicted_objects:
        for predicted_object in predicted_objects[index]:
            # predicted_object.visualize(mat, (255, 255, 255))
            predict_rect = Rectangle(predicted_object.box[0], predicted_object.box[1], predicted_object.box[2],
                                     predicted_object.box[3])
            predict_rect_dict[predicted_object.label['id']].append(predict_rect)
    for key in frame_ious:
        true_area = 0
        predict_area = 0
        frame_over = 0
        true_rects = true_rect_dict[key]
        predict_rects = predict_rect_dict[key]
        union = 0
        union += sum([compute_area(rect) for rect in true_rects])
        union += sum([compute_area(rect) for rect in predict_rects])
        overlap = 0
        for true_rect in true_rects:
            for predict_rect in predict_rects:
                overlap += area(predict_rect, true_rect) * 2
        _overlap[key].append(overlap)
        _union[key].append(union)
        if union > 0:
            frame_ious[key].append(overlap / union)
        else:
            frame_ious[key].append(0)

        ious[key].append(frame_ious[key])

    # cv2.imshow('Video', mat)
    # cv2.waitKey(1)
fps = []
for key in predicted_frames:
    _fps = predicted_frames[key]['fps']
    fps.append(_fps)

print("IOUS : ")
avg_ious = 0
for key in ious:
    if sum(_union[key]) != 0:
        iou = sum(_overlap[key]) / sum(_union[key])
        avg_ious += iou
    else:
        iou = 0
    print('\t{} : {}'.format(traffic_dict[key], iou))
avg_ious = avg_ious / len(ious)
print('\t AVG : {}'.format(avg_ious))

for key in ious:
    for frame in ious[key]:
        for iou_score in frame:
            if iou_score > thresh_hold:
                true_positives[key] += 1
                predict_times[key] += 1
            elif iou_score.__class__ == float:
                false_positives[key] += 1
                predict_times[key] += 1

print("\nPrecision :")
avg_presion = 0
for key in ious:
    if predict_times[key] != 0:
        presion = true_positives[key] / predict_times[key]
        avg_presion += presion
    else:
        presion = 'NA'
    print('\t{} : {}'.format(traffic_dict[key], presion))
avg_presion = avg_presion / len(ious)
# print('\t AVG : {}'.format(avg_presion))
print('\nRecall :')
avg_recall = 0
for key in ious:
    recall = 'NA'
    if annotation_true_positive[key] != 0:
        recall = true_positives[key] / annotation_true_positive[key]
        avg_recall += recall
    print('\t{} : {}'.format(traffic_dict[key], recall))
avg_recall = avg_recall / len(ious)
# print('\t AVG : {}'.format(avg_recall))

# for key in ious:
#     plt.plot(ious[key], label=traffic_dict[key])
#     plt.grid()
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

print('FPS  : {}'.format(sum(fps) / len(fps)))
plt.show()
